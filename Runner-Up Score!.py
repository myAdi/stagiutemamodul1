if __name__ == '__main__':
    max=-100
    min=-100
    n = int(input())
    arr = map(int, input().split())
    for a in arr:
        if a>max:
            min=max
            max=a
        elif a>min and a!=max:
            min=a

print(min)
