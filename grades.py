if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    mylist = []
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()
    for i in student_marks:
        if i==query_name:
            avg=sum(student_marks[i])/float(3)
            print("%.2f" % round(avg,2))
